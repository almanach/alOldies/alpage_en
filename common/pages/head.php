<head>
	<title><?php echo VAR_TITLE ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link href="<?php echo VAR_PATH_TEMPLATES . VAR_TEMPLATE ?>css/main.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo VAR_PATH_TEMPLATES . VAR_TEMPLATE ?>css/graph.css" rel="stylesheet" type="text/css"/>
	<!--CSS spécial IE-->	
	<!-- [if IE]>
	<link href="<?php echo VAR_PATH_TEMPLATES . VAR_TEMPLATE ?>css/ie.css" rel="stylesheet" type="text/css"/>
	<![endif]-->

<!-- 	Functions -->
	<script type="text/javascript" src="./javascript/outils.js"></script>
	<script type="text/javascript" src="./javascript/range.js"></script>
	<script type="text/javascript" src="./javascript/textarea.js"></script>
	<script type="text/javascript" src="./javascript/size.js"></script>
	<script type="text/javascript" src="./javascript/panel.js"></script>
	<script type="text/javascript" src="./javascript/ajax.js"></script>
	

<!--  	Objets-->
	<script type="text/javascript" src="./javascript/ClassParsing.js"></script>
	
<!--  	Get Started !-->
	<script type="text/javascript" src="./javascript/init.js"></script>


</head>
