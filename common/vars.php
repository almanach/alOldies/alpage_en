<?php
// Titre du site :
define('VAR_TITLE','Entités Nommées'); //
// Mail principale :
define('VAR_MAIN_MAIL','mail@préciser'); //

/*
.: CONSTANTES ARCHITECTURALES :.
	index.php fait appel aux pages situées dans /common/pages/
	Ces pages font ensuite appel aux pages dans VAR_PATH_TEMPLATES . VAR_TEMPLATE
		Pour changer l'architecture du site, il faut modifier ce dossier, bien que modifier le CSS directement est également possible.
	Les pages de template font ensuite appel aux pages de VAR_CONTENU, mis dans une constante pour faciliter son édition en cas de besoin : il reste plus simple de l'implémenter tel quel, et de ne changer que le templates.
*/


// Constante du chemin des templates
define('VAR_PATH_TEMPLATES','templates/'); // def : templates/

// Constante permettant d'utiliser un autre template du dossier VAR_PATH_TEMPLATES
define('VAR_TEMPLATE','default/'); // def : default/

// Constante permettant d'utiliser un autre dossier de contenu 
define('VAR_CONTENU','common/contenu/'); //def : common/contenu/


/* 
.: CONSTANTES DES OUTILS :.
*/

// Limite du nombre de caractère de la chaîne à traiter. Préciser 0 pour ne pas limiter (déconseillé)
define('VAR_STRLEN_MAX','1000'); 

// Chemin du script PERL à qui envoyer la chaîne.
define('VAR_PATH_PERL','');


?>