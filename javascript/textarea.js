var currentDiv;
function edit(Obj){
	currentDiv=Obj.innerHTML;

	var editbox=document.getElementById('saisie');
	editbox.style.display='block';
	editbox.focus();
	/* -1 à cause du décalage de la div.
	Notez que si vous souhaitez mettre des bordures à #saisie dans le CSS, vous devez retirer (1 + valeurBordure)
	*/
	editbox.style.width=Obj.offsetWidth-1+'px';
	editbox.style.height=Obj.offsetHeight-1+'px'; 
	editbox.setAttribute('valign','top');
	var str = Obj.innerHTML.replace(new RegExp("&nbsp;", "g"),'');
	Obj.innerHTML = "";
	editbox.style.position='absolute';

	editbox.value= str;
}
 
function copyValue(ta){
	//TransValue="";
	if(ta.value.length>0) var val = ta.value;
	else var val = "&nbsp;";
	ta.style.display='none';
	currentDiv = ta.value;
	document.getElementById('miror').innerHTML=currentDiv;
}

// note sur les occurences :
/* Une occurences à un id comme ceci :
id = "EN idEN varLieu"
EN : constante, permet de savoir que c'est une Entité Nommée
idEn : identifiant permettant de grouper les EN devant l'être.
varLieu : {panel, render}. Assure l'unicité.
*/
function parserT() {
	// Nettoyage du panel.
	emptyNode(document.getElementById('liste_en'));
	// Pour le moment, le corpus est en XML, en attendant mieux ^^
	var p = new Parsing();
	p.initFromXMLString(trim(currentDiv));
	//alert(str_output);
	for(var i=0;i<document.forms.f_parser.type_sortie.length;++i) {
		if(document.forms.f_parser.type_sortie[i].checked == true) {
			var ts = document.forms.f_parser.type_sortie[i].value;
			break;
		}
	}
	if(!ts) {
		alert("Erreur dans la sortie demandée");
		return "ERREUR !";
	}
	if(console) console.log("Sortie demandée : "+ ts);
	switch(ts) {
		case 'seq' :
			document.getElementById('left').setAttribute('class','seq');
			document.getElementById('panel').setAttribute('class','seq');
			var str_output = p.dispPhraseEN();
			break;
		case 'gra' :
			document.getElementById('left').setAttribute('class','');
			document.getElementById('panel').setAttribute('class','');
			var str_output = p.dispGraphEN().innerHTML;
			break;
		default :
			var str_output = "ERREUR !";
			break;
	}
	document.getElementById('render').innerHTML = str_output;
	
   	feetHSmallCol('left', 'panel');

	
	var EN = getElementsByClassName('occ','',document.getElementById('render'));
	
	
	for(var i=0;i<EN.length;++i) {
		EN[i].onclick = function() {findOccInPanel(this)}
	}

	var tdeplier = (getElementsByClassName("depliable","",document.getElementById("liste_en")));
	for(var i=0;i<tdeplier.length;++i) {
		tdeplier[i].onclick=function(){xplier(this);}
	}
	document.getElementById('tout_replier').onclick=function(){return xplierTout('replier',this)}
	document.getElementById('tout_deplier').onclick=function(){return xplierTout('deplier',this)}
}

function emptyNode(n) {
	while(n.firstChild) {
		n.removeChild(n.firstChild);
	}
}

function reset() {
	document.getElementById('render').innerHTML = "&nbsp;";
	document.getElementById('miror').innerHTML = "&nbsp;";
	emptyNode(document.getElementById('liste_en'));
	currentDiv = "";
	//feetHHightCol('left', 'panel');
	return false;
}

function findOccInPanel(d) {
	if(!d.id || (d.id.split(" ")[0] != 'EN'))return false;
	else {
		var occ = d.id.split(" ")[1];
		var ten = getElementsByClassName("EN","div",document.getElementById("liste_en"));
		//alert(ten[occ].innerHTML +" ||| " + occ);
		for(var i=0;i<ten.length;++i) {
			var ti = ten[i].id.split(" ");
			if(occ == ti[1]) {
				highlight(document.getElementById(ten[i].id + " panel"));
				xplier(document.getElementById("title " + ten[i].id));
				break;
			}
		}
	}
}
