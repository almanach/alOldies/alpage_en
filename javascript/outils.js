function calcBgById(idE) {
	var rand = idE.split("_");
	var rand = parseInt(rand[2] * rand[1]);
	var vr = 0;
	var g = ((rand+1)*2) % 255;
	var b = ((rand+1)-(rand*5)) % 255;
	do {
		vr++;
		var r = ((rand+vr)*200)%255;
	}while((r < 100) && (g < 100) && (b < 100));
	return "rgb("+ r +","+ g + "," + b + ")";
}
function getElementsByClassName(className, tag, elm){
	var testClass = new RegExp("(^|s)" + className + "(s|$)");
	var tag = tag || "*";
	var elm = elm || document;
	var elements = (tag == "*" && elm.all)? elm.all : elm.getElementsByTagName(tag);
	var returnElements = [];
	var current;
	var length = elements.length;
	for(var i=0; i<length; i++){
		current = elements[i ];
		if(testClass.test(current.className)){
			returnElements.push(current);
		}
	}
	return returnElements;
}
/* Créer une div :
	src = div mère
	id = id de la nouvelle div
	classN = nom de la classe
	tStyle = tableau de style
	inner = contenu de la div

*/
function createDiv(src, id, classN, tStyle, inner) {
	src = document.getElementById(src);
	var nDiv = document.createElement('div');
	nDiv.setAttribute('id',id);
	if(classN) nDiv.setAttribute('class',classN);
	for(var i = 0; i < tStyle.length; ++i) {
		var st = 'nDiv.style.' + tStyle[i][0] + ' = "' + tStyle[i][1] + '"';
		eval(st);
	}
	if(inner) nDiv.innerHTML = inner;
	src.appendChild(nDiv);
	return nDiv;
}

// Créer le tableau de styles
function createTStyle(t) {
	var temp = new Array();
	for(var i = 0, j = 0; i < t.length; i=i+2, j++) {
		temp[j] = new Array(t[i],t[i+1]);
	}
	return temp;
}

// Evite l'autorisation de balises dans la div
function htmlspecialchars(ch) {
   //ch = ch.replace(/&/g,"&amp;");
   ch = ch.replace(/\"/g,"&quot;");
   ch = ch.replace(/\'/g,"&#039;");
   ch = ch.replace(/</g,"&lt;");
   ch = ch.replace(/>/g,"&gt;");
   return ch;
}

// supprime les espace en fin et en début de chaîne.
function trim(ch) {
	return ch.replace(/^\s*|\s*$/,"");
}

// Récupérer le texte selectionné : tout navigateur.
/*
function txtsel() {
	if(window.selection) return "1" + window.selection();
	if (document.getSelection) return "2" + document.getSelection;
	if(window.getSelection) return "3" + window.getSelection();
	if (document.selection) return "4" + document.selection.createRange().text;
	return false;
}*/
