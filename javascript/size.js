﻿/*
	Firefox ignorant l'événement onresize, il faut donc coder une fonction qui s'occupe de surveiller la taille des div.

*/

function feetHCols(idSource, idTarget) {
	return document.getElementById(idTarget).style.height = document.getElementById(idSource).offsetHeight + 'px';
}

function feetHSmallCol(idCol1, idCol2) {
	if(document.getElementById(idCol1).offsetHeight == document.getElementById(idCol2).offsetHeight) return true;
	if(document.getElementById(idCol1).offsetHeight > document.getElementById(idCol2).offsetHeight) return feetHCols(idCol1,idCol2);
	else return feetHCols(idCol2, idCol1);

}

function feetHHightCol(idCol1, idCol2) {
	if(document.getElementById(idCol1).offsetHeight == document.getElementById(idCol2).offsetHeight) return true;
	if(document.getElementById(idCol1).offsetHeight < document.getElementById(idCol2).offsetHeight) return feetHCols(idCol1,idCol2);
	else return feetHCols(idCol2, idCol1);

}

function resize() {
	feetHSmallCol('left', 'panel');
}
function resizeFF() {
	//alert('Ok');
	setTimeout("resizeFF();",500);
	feetHSmallCol('left', 'panel');
}