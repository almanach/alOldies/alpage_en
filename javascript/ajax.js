/*
	Fonctions AJAX
*/
function connex() {
	if(window.XMLHttpRequest) // Firefox 
		return new XMLHttpRequest(); 
	else if(window.ActiveXObject) // Internet Explorer 
		return ActiveXObject("Microsoft.XMLHTTP"); 
	else { // XMLHttpRequest non supporté par le navigateur 
		alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
		return; 
	}
}

function open(xhr,fic,p) {
	xhr.onreadystatechange  = function() { 
		if(xhr.readyState  == 4) {
			if(xhr.status  == 200) {
				p.initFromXMLString(xhr.responseText);
			}
			else return "Erreur !";
		}
	}; 

	xhr.open("GET", fic,  true); 
	xhr.send(null);
}

// Function d'émulation d'asynchronisme : permet d'attendre le résultat d'une requête.
/*
nonNul : expression à vérifier qui stoppera la boucle lorsqu'elle ne sera plus nulle. 
func : fonction à lancer une fois le résultat positif
*/
var GLOBAL_TIMEOUT;
function wait(nonNul, launch, timeout, interval) {
	var tps = interval || 100;
	var to = timeout || 1000;
	GLOBAL_TIMEOUT=0;
	wait_(nonNul,launch,to,tps);
}

function wait_(nonNul,launch,timeout,interval) {
	GLOBAL_TIMEOUT++;
	if(console) console.log(GLOBAL_TIMEOUT);
	if(GLOBAL_TIMEOUT * interval > timeout) return false;
	if(nonNul == 0) setTimeout("wait_(p.init_,'chose',"+timeout+","+interval+")",interval);
	else eval(launch+"()");
	
}
