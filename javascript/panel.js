function xplier(enPanel) {
	if(enPanel.getAttribute('class') == 'depliable') {
		enPanel.setAttribute('class','repliable');
		enPanel.innerHTML = "-" + enPanel.innerHTML.substring(1);
		enPanel.parentNode.lastChild.style.display = "";

		document.getElementById('panel').style.height = (document.getElementById('titre_panel').offsetHeight + document.getElementById('pliement').offsetHeight + document.getElementById('liste_en').offsetHeight) + "px";
		feetHSmallCol('panel', 'left');
		feetHSmallCol('panel', 'left');
	}
	else {
		enPanel.setAttribute('class','depliable');
		enPanel.innerHTML = "+" + enPanel.innerHTML.substring(1);		
		enPanel.parentNode.lastChild.style.display = "none";
	}
	return 1;
}

function xplierTout(action) {
	if(action == "deplier") {
		tTemp = getElementsByClassName('depliable',"",document.getElementById('panel'));
		tDiv = new Array();
		for(i=0;i<tTemp.length;++i) {
			tDiv[i] = tTemp[i];
		}
		for(var i=0;i<tDiv.length;++i) {
			xplier(tDiv[i]);
		}
		
	}
	if(action == "replier") {
		tTemp = getElementsByClassName('repliable',"",document.getElementById('panel'));
		tDiv = new Array();
		for(i=0;i<tTemp.length;++i) {
			tDiv[i] = tTemp[i];
		}
		for(var i=0;i<tDiv.length;++i) {
			xplier(tDiv[i]);
		}
	}
}

function highlight(d) {
	var tc = document.getElementById('render').childNodes;
	// les occurences sont stockées dans les id
	var tocc = d.id.split(' ');
	// L'occurence est-elle une EN ?
	if(tocc[0] == 'EN') {
		var nbOcc = 0;
		var bg;
		for(var i=0;i<tc.length;++i) {
			if(tc[i].id) {
				// on split l'id.
				var ttc = tc[i].id.split(' ');
				if(ttc[0] != 'EN') break;
				//alert(ttc[1] + " / " + tocc[1]);
				if(ttc[1] == tocc[1]) {
					//rgb("+((occ+1)*200)%255 +","+ ((occ)*2) % 255 + "," + ((occ+1)*(occ*5)) % 255 + ")"
					if(!bg) { 
 						var bg = calcBgById(tocc[1]);
					}
					//alert(bg);
					tc[i].style.backgroundColor = bg;
					d.parentNode.parentNode.style.backgroundColor = bg;
					nbOcc++;
				}
			}
		}
		if(nbOcc) {
			d.innerHTML = "[Libérer]";
			d.onclick = function() {unHighlight(this);}
		}
		else alert("Aucun élément trouvable dans le texte."); // Normalement, impossible d'arriver ici.
	}
	else return false;
}

function unHighlight(d) {
	var tocc = d.id.split(' ');
	var tc = document.getElementById('render').childNodes;
	var nbOcc = 0;
	for(var i=0;i<tc.length;++i) {
		if(tc[i].id) {
			var ttc = tc[i].id.split(' ');
			if(ttc[1] == tocc[1]) {
				tc[i].style.backgroundColor = "";
				d.parentNode.parentNode.style.backgroundColor = "";
				nbOcc++;
			}
		}
	}
	if(nbOcc) {
		d.innerHTML = "[Occurences]";
		d.onclick = function() {highlight(this);}
	}
}

// TODO : 
function highlightTout() {
	var tc = getElementsByClassName('occ').childNodes;
	alert('');
}