function getRange() {
	// Ne pas déclencher la sélection pour un clique vide :
	if(document.selection) { // Opéra + IE
		return document.selection.createRange();
	}
	if(window.getSelection) { // Firefox (https://developer.mozilla.org/En/DOM:Selection)
		return window.getSelection();
	}
	/*if(window.selection) {
		if(window.selection() == "") return false;
		return "03" + window.selection;
	}*/
	if(document.getSelection) { // NS 4
		return document.getSelection();
	}
	return false;
}