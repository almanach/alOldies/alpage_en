function Parsing() {
	this.clusters_ = new Array();
	this.nodes_ = new Array();
	this.edges_ = new Array();
	this.ops_ = new Array();

	// prêt ?
	this.init_ = false;
	
	// Tableau des lemmas correspondants à des EN
	this.tEnByLemma_ = new Array("_ADRESSE","_DATE_artf");

	// Adapté depuis http://www.w3schools.com
	this.initFromXMLString = function (xml){
		try {
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async="false";
		xmlDoc.loadXML(xml);
		} 
		catch(e) {
			//Firefox, Mozilla, Opera, etc.
			try {
				parser=new DOMParser();
				xmlDoc=parser.parseFromString(xml,"text/xml");
			}
			catch(e) {
				alert("Erreur : impossible d'accèder à la chaîne XML");
				return;
			}
		}
		var children = xmlDoc.documentElement.childNodes;

		for (var i=0;i<children.length;i++) { 
			if (children[i].nodeType==1) {
				switch (children[i].nodeName) {
					case "cluster" :
						if(!this.clusters_.push(children[i])) return "ERREUR !";
						break;
					case "node" :
						if(!this.nodes_.push(children[i])) return "ERREUR !";
						break;
					case "edge" :
						if(!this.edges_.push(children[i])) return "ERREUR !";
						break;
					case "op" :
						if(!this.ops_.push(children[i])) return "ERREUR !";
						break;
					default :
						break;
				}
			}
		}
		this.init_ = true;
		return true;
		
//		document.write(this.dispPhraseSimple());
// 		document.write(this.sumNodeArray(this.clusters_));
// 		document.write(this.sumNodeArray(this.nodes_));
// 		document.write(this.sumNodeArray(this.edges_));
// 		document.write(this.sumNodeArray(this.ops_));
	}
	
	this.sumNodeArray = function (a){
		var str = "";
		str += a[0].nodeName + " ("+a.length+") : <br />";
		for(var i = 0; i<a.length;++i) {
			str += "&nbsp;&nbsp;&nbsp;" + i + "/ ";
			str += "Attributs : " + a[i].attributes.length + " = ";
			for(var j=0;j<a[i].attributes.length;++j) {
				str += a[i].attributes[j].name + ":" + a[i].attributes[j].value + " - ";
			}
			str += "["+ a[i].childNodes.length +"]"
			str += "<br />";
		}
		return str;
	}
	
	this.dispPhraseSimple = function () {
		//reconstruction de la phrase
		var p = "";
		for(var i=0;i<this.clusters_.length;++i) {
			for(var j=0;j<this.clusters_.length;++j) {
				if(this.clusters_[j].attributes.getNamedItem('id').nodeValue == "c_" + i + "_"+ (i+1)) {
					p += this.clusters_[j].attributes.getNamedItem('lex').nodeValue;
					break;
				}
				//else p+= ("c_" + i + "_"+ i+1);
			}
			p += " ";
		}
		return p;
	}

	this.isEnByLemma = function (lemma) {
		for(var i=0;i<this.tEnByLemma_.length;++i) {
			if(lemma == this.tEnByLemma_[i]) return true;
		}
		return false;
	}

	this.dispPhraseEN = function () {

		//reconstruction de la phrase
		var p = "";
		for(var i=0;i<this.clusters_.length;++i) {
			for(var j=0;j<this.clusters_.length;++j) {
				if(this.clusters_[j].attributes.getNamedItem('id').nodeValue == "c_" + i + "_"+ (i+1)) {
					// Trouvé.
					var k=0;
					for(;k<this.nodes_.length;++k) {
						if(this.nodes_[k].attributes.getNamedItem('cluster').nodeValue == "c_" + i + "_"+ (i+1)) {
							break;
						}
					}
					if(this.nodes_[k].attributes.getNamedItem('cat').nodeValue != '_') p+= " "
					
					if(this.nodes_[k].attributes.getNamedItem('cat').nodeValue == 'np' || this.isEnByLemma(this.nodes_[k].attributes.getNamedItem('lemma').nodeValue)) {
						p += '<div id="EN ';
						p += this.clusters_[j].attributes.getNamedItem('id').nodeValue+' render';
						p += '" class="occ">'+this.clusters_[j].attributes.getNamedItem('lex').nodeValue;
						p += '</div>';
						this.dispENPanel(this.nodes_[k].attributes,this.clusters_[j].attributes);
					}
					else { 
						p += this.clusters_[j].attributes.getNamedItem('lex').nodeValue;
						break;
					}
					break;
				}
			}
		}
		return p;
	}
	this.dispGraphEN = function() {
		// Création d'une liste de cluster en tant qu'entité HTML (div)
		var d = document.createElement('div');
		// On remplit cette entité d'autant de cluster, qui seront ensuite eux-mêmes remplis de nodes
		for(var i=0;i<this.clusters_.length;++i) {
			var dc = document.createElement('div');
			d.appendChild(dc);
			dc.setAttribute('class','g_cluster');
			var dcid = this.clusters_[i].attributes.getNamedItem('id').nodeValue;
			dc.setAttribute('id','graph '+dcid);
			var titre_cluster = document.createElement('h3');
			titre_cluster.innerHTML = this.clusters_[i].attributes.getNamedItem('lex').nodeValue;
			dc.appendChild(titre_cluster);
			// Pour chacun de ces clusters, il faut vérifier quels sont les nodes associés.
			for(var j=0;j<this.nodes_.length;j++) {
				if(dcid == this.nodes_[j].attributes.getNamedItem('cluster').nodeValue) {
					var dn = document.createElement('div');
					dc.appendChild(dn);
					dn.setAttribute('id','graph ' + this.nodes_[j].attributes.getNamedItem('id').nodeValue);
					dn.setAttribute('class','g_node');
					dn.innerHTML = this.nodes_[j].attributes.getNamedItem('lemma').nodeValue + " (" + this.nodes_[j].attributes.getNamedItem('cat').nodeValue +")";
				}
			}
		}
		return d;
	
	}
	
	
	this.dispENPanel = function (tAttNode,tAttCluster) {
		var str = "";
		var temp = createDiv("liste_en","EN " + tAttCluster.getNamedItem('id').nodeValue, "EN", new Array(), "");
		var temp_title = createDiv(temp.id, "title " + temp.id, "depliable", new Array(), "+ " + tAttCluster.getNamedItem('lex').nodeValue);
		var temp_descr = createDiv(temp.id, "descr " + temp.id, "description",createTStyle(new Array('display', 'none')),"");
		
		// Ce que l'on met dans tout ça : 
		// Le highlight
		createDiv(temp_descr.id, temp.id+" panel", "occ",new Array(),"[Occurences]").onclick = function() {highlight(this);};
		// Id
		createDiv(temp_descr.id, "id " + temp.id, "", new Array(), "(" + tAttCluster.getNamedItem('id').nodeValue + ")");
		// Catégorie
		createDiv(temp_descr.id, "cat " + temp.id, "", new Array(), tAttNode.getNamedItem('lemma').nodeValue);
		// Edges targets
		/*
		for(var i=0;i<this.edges_.length;++i) {	
		// Tableau temporaire
			var ttemp = new Array();
			if(tAttNode.getNamedItem('id').nodeValue == this.edges_[i].attributes.getNamedItem('target').nodeValue) {
				//if(console) console.log('avec en target : ' +  this.edges_[i].attributes.getNamedItem('target').nodeValue);
				for(var j=0;j<this.nodes_.length;++j) {
					if(this.nodes_[j].attributes.getNamedItem('id').nodeValue == this.edges_[i].attributes.getNamedItem('target').nodeValue) {
						for(var k=0;k<this.clusters_.length;++k) {
							if(this.clusters_[k].attributes.getNamedItem('id').nodeValue == this.nodes_[j].attributes.getNamedItem('cluster').nodeValue) {
								createDiv(temp_descr.id, "target " + temp.id + " " + i, "", new Array(), ("est target de : '"+this.clusters_[k].attributes.getNamedItem('lex').nodeValue + "' par " + this.nodes_[j].attributes.getNamedItem('lemma').nodeValue));
								if(console) {
									var msg = "i:" + i;
									msg += "|edge:"+this.edges_[i].attributes.getNamedItem('id').nodeValue;
									
									msg += "\nj:" + j;
									msg += "|node:"+this.nodes_[j].attributes.getNamedItem('id').nodeValue;
																		
									msg += "\nk:" + k;
									msg += "|cluster:"+this.clusters_[k].attributes.getNamedItem('id').nodeValue;
									
									console.log(msg);								
								}
								
							}
						}
								//ttemp[j].attributes.getNamedItem('cluster').nodeValue = "Kikoo";
								//console.log(ttemp[j].attributes.getNamedItem('cluster').nodeValue);
					}
				}
			}
		}*/
	}
}
